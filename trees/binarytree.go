package trees

import "fmt"

type Node struct {
	data  int
	left  *Node
	right *Node
}

type BinaryTree struct {
	root  *Node
}

func (t *BinaryTree) AddNode(data int) {
	t.root = insert(t.root, data)
}

func insert(root *Node, data int) *Node {
	if root == nil {
		return &Node{data:data}
	}	
	
	if data > root.data {
        root.right = insert(root.right, data)
	} else if data < root.data {
        root.left = insert(root.left, data);
	}
	
	return root
}

func (t *BinaryTree) Delete(data int) {
	delete(t.root, data)
}

func minValueNode(node *Node) *Node {
    current := node

    for current != nil && current.left != nil {
        current = current.left
	}
	
    return current
}

func delete(root *Node, data int) *Node {
    if root == nil {
        return root
	}

    if data < root.data {
        root.left = delete(root.left, data)
	} else if data > root.data {
        root.right = delete(root.right, data)
	} else {
        if root.left == nil && root.right == nil {
            return nil
		} else if root.left == nil {
            temp := root.right
            return temp
        } else if root.right == nil {
            temp := root.left
            return temp
        }

        temp := minValueNode(root.right)

        root.data = temp.data
        root.right = delete(root.right, temp.data)
    }
    return root
}

func (t *BinaryTree) PreOrderTraversal() {
	preorder(t.root)
}

func preorder(focusNode *Node) {
	if focusNode != nil {
		fmt.Printf("%d\n", focusNode.data)
		preorder(focusNode.left)
		preorder(focusNode.right)
	}
}

func (t *BinaryTree) InOrderTraversal() {
	inorder(t.root)
}

func inorder(focusNode *Node) {
	if focusNode != nil {
		inorder(focusNode.left)
		fmt.Printf("%d\n", focusNode.data)
		inorder(focusNode.right)
	}
}