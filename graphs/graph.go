package graphs

import (
	"fmt"
)

// Node represents a node in the graph.
type Node struct {
	ID    int
	Value interface{}
}

// Edge represents an edge between two nodes in the graph.
type Edge struct {
	Source *Node
	Target *Node
}

// Graph represents a graph with nodes and edges.
type Graph struct {
	Nodes []*Node
	Edges []*Edge
}

// NewNode creates a new node with the given ID and value.
func NewNode(id int, value interface{}) *Node {
	return &Node{ID: id, Value: value}
}

// NewEdge creates a new edge between two nodes.
func NewEdge(source, target *Node) *Edge {
	return &Edge{Source: source, Target: target}
}

// AddNode adds a node to the graph.
func (g *Graph) AddNode(node *Node) {
	g.Nodes = append(g.Nodes, node)
}

// AddEdge adds an edge to the graph.
func (g *Graph) AddEdge(edge *Edge) {
	g.Edges = append(g.Edges, edge)
}

// PrintGraph prints the nodes and edges of the graph.
func (g *Graph) PrintGraph() {
	fmt.Println("Nodes:")
	for _, node := range g.Nodes {
		fmt.Printf("Node %d: %v\n", node.ID, node.Value)
	}

	fmt.Println("Edges:")
	for _, edge := range g.Edges {
		fmt.Printf("Edge from Node %d to Node %d\n", edge.Source.ID, edge.Target.ID)
	}
}
