package lists

import "fmt"

type Node struct {
    value int
    next *Node
}

type LinkedList struct {
	head *Node
}

func (l LinkedList) Print() {
	current := l.head
	
	for current != nil {
        fmt.Printf("%d\n", current.value)
        current = current.next
    }
}

// Last element

func (l *LinkedList) Push(value int) {
	if l.head == nil {
		l.head = &Node{value:value}
		return
	}
	current := l.head
	
	for current.next != nil {
		current = current.next
	}
	
	current.next = &Node{value:value}
}

func (l *LinkedList) Unpush() int {
	if l.head == nil {
		return -1
	}
	current := l.head
	
	for current.next.next != nil {
		current = current.next
	}
	
	value := current.next.value
	current.next = nil
	
	return value
}

func (l *LinkedList) Remove_last() {
	if l.head == nil {
		// early return, nothing to do
		return
	}
	current := l.head
	
	for current.next.next != nil {
		current = current.next
	}
	
	current.next = nil
}

// First element

func (l *LinkedList) Unpop(value int) {
	new_node := &Node{value: value, next: l.head}
	l.head = new_node
}

func (l *LinkedList) Pop() int {
	if l.head == nil {
		// early return, nothing to do
		return -1
	}
	
	popped_node := l.head
	l.head = popped_node.next
	
	return popped_node.value
}

func (l *LinkedList) Remove_first() {
	if l.head == nil {
		// early return, nothing to do
		return
	}
	
	next_node := l.head.next
	l.head = next_node
}

// Random access

func (l *LinkedList) Remove_by_index(j int) int {
	if l.head == nil {
		// early return, nothing to do
		return -1
	}
	current := l.head
	
	for i:=0;i<=j-2;i++ {
		current = current.next
	}
	
	temp_node := current.next
	current.next = temp_node.next
	
	return temp_node.value
}

func (l *LinkedList) Remove_by_value(v int) int {
	if l.head == nil {
		// early return, nothing to do
		return -1
	}
	current := l.head
	found := false
	
	for !found {
		if current.next.value == v {
			found = true
		} else {
			current = current.next
		}
	}
	
	temp_node := current.next
	current.next = temp_node.next
	
	return temp_node.value
}
