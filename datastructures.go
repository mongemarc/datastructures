package main

import (
	"datastructures/graphs"
	"datastructures/lists"
	"datastructures/trees"
	"fmt"
	"strconv"
)

func main() {
	list := lists.LinkedList{}
	list.Push(7)
	list.Push(11)
	list.Unpop(5)
	list.Unpop(3)
	list.Unpop(2)
	list.Print()
	fmt.Printf("--do pop---\n")
	value := list.Pop()
	list.Print()
	fmt.Printf("--value---")
	fmt.Printf(strconv.Itoa(value) + "\n")
	fmt.Printf("--do unpush---\n")
	value = list.Unpush()
	list.Print()
	fmt.Printf("--value---")
	fmt.Printf(strconv.Itoa(value) + "\n")

	list.Push(13)
	list.Push(17)
	fmt.Printf("--remove item 1 (5)---\n")
	list.Remove_by_index(1)
	list.Print()

	list.Push(19)
	list.Push(23)
	fmt.Printf("--remove value 17---\n")
	list.Remove_by_value(17)
	list.Print()

	fmt.Printf("----------TREE-----------\n")
	tree := trees.BinaryTree{}

	tree.AddNode(11)
	tree.AddNode(17)
	tree.AddNode(13)
	tree.AddNode(7)
	tree.AddNode(3)
	tree.AddNode(2)
	tree.AddNode(5)

	tree.Delete(7)

	tree.InOrderTraversal()

	// Create a new graph
	graph := &graphs.Graph{}

	// Create nodes
	node1 := graphs.NewNode(1, "Node 1")
	node2 := graphs.NewNode(2, "Node 2")
	node3 := graphs.NewNode(3, "Node 3")

	// Add nodes to the graph
	graph.AddNode(node1)
	graph.AddNode(node2)
	graph.AddNode(node3)

	// Create edges
	edge1 := graphs.NewEdge(node1, node2)
	edge2 := graphs.NewEdge(node2, node3)

	// Add edges to the graph
	graph.AddEdge(edge1)
	graph.AddEdge(edge2)

	// Print the graph
	graph.PrintGraph()
}
